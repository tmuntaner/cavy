# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cavy_item, class: Cavy::Item do
    data {}
  end
end
